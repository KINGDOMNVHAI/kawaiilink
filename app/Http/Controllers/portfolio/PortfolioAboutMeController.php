<?php
namespace App\Http\Controllers\portfolio;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Session;

class PortfolioAboutMeController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $title = 'Hello World';
        return view('portfolio-about-me.pages.home', [
            'title' => $title,
        ]);
    }
}
