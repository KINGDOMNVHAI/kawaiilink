<?php
namespace App\Http\Controllers\main;

use App\Http\Controllers\Controller;
use App\Services\CustomerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Session;

class MainController extends Controller
{
    public function __construct()
    {

    }

    public function locale($locale)
    {
        // Session::set('locale', $locale);
        // $request->session->set('locale', $locale);

        if (! in_array($locale, ['en','vi'])) {
            abort(400);
        }
        App::setLocale($locale);
        session()->put('locale', $locale);

        // return redirect()->back();
        return redirect()->route('main-home');
    }

    public function index()
    {
        $title = 'Make a beauty (kawaii) profile';

        $language = session()->get('locale', config('app.locale'));
        // Lấy dữ liệu lưu trong Session, không có thì trả về default lấy trong config
        config(['app.locale' => $language]);
        // Chuyển ứng dụng sang ngôn ngữ được chọn

        return view('main.pages.home', [
            'title' => $title
        ]);
    }

    public function demo()
    {
        // Luôn luôn trong trạng thái lấy data từ các input trong form dù có search hay không
        $request = Request::capture();
        $customerService = new CustomerService();
        $customerService->insertWalkInGuest($request);

        $fullname = 'human (ningen)';
        if ($request->fullname != null) {
            $fullname = $request->fullname;
        }

        $title = 'Welcome to your beauty (kawaii) demo, ' . $fullname;
        $welcome = 'Welcome to your beauty (kawaii) demo';
        $slogan = 'Your Slogan';

        $embedcode = array(
            '<iframe allowfullscreen="" frameborder="0" width="100%" height="550" src="https://player.vimeo.com/video/705946811?h=a7993aeb9d" title="vimeo-player"></iframe>',
            '<blockquote class="tiktok-embed" cite="https://www.tiktok.com/@arinyan_sekapin/video/7011916904155696386" data-video-id="7011916904155696386" style="max-width: 605px;min-width: 325px;" > <section> <a target="_blank" title="@arinyan_sekapin" href="https://www.tiktok.com/@arinyan_sekapin">@arinyan_sekapin</a> 温泉行きてええうぁぁぁぇえぇぇぇ<a title="youtubeいつもありがとうな" target="_blank" href="https://www.tiktok.com/tag/youtube%E3%81%84%E3%81%A4%E3%82%82%E3%81%82%E3%82%8A%E3%81%8C%E3%81%A8%E3%81%86%E3%81%AA">#youtubeいつもありがとうな</a> <a target="_blank" title="♬ オリジナル楽曲  - ありにゃん" href="https://www.tiktok.com/music/オリジナル楽曲-ありにゃん-7011916824321346306">♬ オリジナル楽曲  - ありにゃん</a> </section> </blockquote> <script async src="https://www.tiktok.com/embed.js"></script>',
            '<blockquote class="tiktok-embed" cite="https://www.tiktok.com/@skm__0018/video/7062609915973537026" data-video-id="7062609915973537026" style="max-width: 605px;min-width: 325px;" > <section> <a target="_blank" title="@skm__0018" href="https://www.tiktok.com/@skm__0018">@skm__0018</a> <a title="ビキニ" target="_blank" href="https://www.tiktok.com/tag/%E3%83%93%E3%82%AD%E3%83%8B">#ビキニ</a> <a title="雪" target="_blank" href="https://www.tiktok.com/tag/%E9%9B%AA">#雪</a> <a title="温泉" target="_blank" href="https://www.tiktok.com/tag/%E6%B8%A9%E6%B3%89">#温泉</a> <a target="_blank" title="♬ kitty kat by megan - wnohyuk" href="https://www.tiktok.com/music/kitty-kat-by-megan-7033124079553202990">♬ kitty kat by megan - wnohyuk</a> </section> </blockquote> <script async src="https://www.tiktok.com/embed.js"></script>',
            '<iframe width="100%" height="550" src="https://www.youtube.com/embed/11gLVTXFBYQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
            '<blockquote class="tiktok-embed" cite="https://www.tiktok.com/@conan0729/video/6749470834533977346" data-video-id="6749470834533977346" style="max-width: 605px;min-width: 325px;" > <section> <a target="_blank" title="@conan0729" href="https://www.tiktok.com/@conan0729">@conan0729</a> ちょうど当たってムズムズするね🤣<a title="犬" target="_blank" href="https://www.tiktok.com/tag/%E7%8A%AC">#犬</a><a title="チワックス" target="_blank" href="https://www.tiktok.com/tag/%E3%83%81%E3%83%AF%E3%83%83%E3%82%AF%E3%82%B9">#チワックス</a><a title="子犬" target="_blank" href="https://www.tiktok.com/tag/%E5%AD%90%E7%8A%AC">#子犬</a><a title="子犬のいる生活" target="_blank" href="https://www.tiktok.com/tag/%E5%AD%90%E7%8A%AC%E3%81%AE%E3%81%84%E3%82%8B%E7%94%9F%E6%B4%BB">#子犬のいる生活</a> <a target="_blank" title="♬ 皇家萌卫 - GTech" href="https://www.tiktok.com/music/皇家萌卫-6462177439296293646">♬ 皇家萌卫 - GTech</a> </section> </blockquote> <script async src="https://www.tiktok.com/embed.js"></script>',
            '<blockquote class="tiktok-embed" cite="https://www.tiktok.com/@impriyaanka_/video/6995513886589897985" data-video-id="6995513886589897985" style="max-width: 605px;min-width: 325px;" > <section> <a target="_blank" title="@impriyaanka_" href="https://www.tiktok.com/@impriyaanka_">@impriyaanka_</a> Nothing feels better than this 💦✨ <a title="better" target="_blank" href="https://www.tiktok.com/tag/better">#better</a> <a title="nz" target="_blank" href="https://www.tiktok.com/tag/nz">#nz</a> <a title="queenstown" target="_blank" href="https://www.tiktok.com/tag/queenstown">#queenstown</a> <a title="onsen" target="_blank" href="https://www.tiktok.com/tag/onsen">#onsen</a></section> </blockquote> <script async src="https://www.tiktok.com/embed.js"></script>',
            '<blockquote class="tiktok-embed" cite="https://www.tiktok.com/@todaysport/video/7089629790373743898" data-video-id="7089629790373743898" style="max-width: 605px;min-width: 325px;" > <section> <a target="_blank" title="@todaysport" href="https://www.tiktok.com/@todaysport">@todaysport</a> Football Funny Moments Part -1 <a title="foryoupage" target="_blank" href="https://www.tiktok.com/tag/foryoupage">#foryoupage</a> <a title="foryou" target="_blank" href="https://www.tiktok.com/tag/foryou">#foryou</a> <a title="football" target="_blank" href="https://www.tiktok.com/tag/football">#football</a> <a title="footballmyanmar" target="_blank" href="https://www.tiktok.com/tag/footballmyanmar">#footballmyanmar</a> <a title="savetheworld" target="_blank" href="https://www.tiktok.com/tag/savetheworld">#savetheworld</a> <a target="_blank" title="♬ original sound  - ᎢᎾᎠᎪY ᏚᏢᎾᎡᎢ" href="https://www.tiktok.com/music/original-sound-ᎢᎾᎠᎪY-ᏚᏢᎾᎡᎢ-7089629809747200794">♬ original sound  - ᎢᎾᎠᎪY ᏚᏢᎾᎡᎢ</a> </section> </blockquote> <script async src="https://www.tiktok.com/embed.js"></script>'
        );
        $size = sizeof($embedcode);
        $randNumber = rand(0, $size - 1);

        return view('demo2.master-demo', [
            'title' => $title,
            'welcome' => $welcome,
            'slogan' => $slogan,
            'fullname' => $fullname,
            'embedcode' => $embedcode[$randNumber]
        ]);
    }
}
