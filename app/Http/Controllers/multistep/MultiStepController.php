<?php
namespace App\Http\Controllers\multistep;

use App\Http\Controllers\Controller;
use App\Services\CustomerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class MultiStepController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $title = 'Register';
        return view('multi-step.pages.step', [
            'title' => $title
        ]);
    }

    public function register(Request $request)
    {
        // Validation
        $this->validate($request, [
            'cfPassword' => 'min:6|required_with:cfCheckPassword|same:cfCheckPassword',
            'cfCheckPassword' => 'min:6'
        ]);

        $customerService = new CustomerService();
        $customer = $customerService->insertCustomer($request);
        // if ($customer->id != null) {

        // }

        $title = 'Send email';

        return view('multi-step.pages.send-email', [
            'title' => $title
        ]);
    }
}
