<?php
namespace App\Services;

use Illuminate\Support\ServiceProvider;
use App\Models\User;
use App\Models\UserLinks;
use App\Models\WalkInGuest;
use App\Ultis\StringUltis;
use DB;

class CustomerService extends ServiceProvider
{
    public function __construct()
    {

    }

    /**
     * Insert Walk-In Guest
     *
     * @return array
     */
    public function insertWalkInGuest($request)
    {
        return WalkInGuest::insert([
            'name_wig' => $request->fullname,
            'country_wig' => $request->country,
            'email_wig' => $request->email,
            'phone_wig' => $request->phone,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * Insert Customer
     *
     * @return array
     */
    public function insertCustomer($request)
    {
        User::insert([
            'firstname' => $request->cfFirstName,
            'lastname' => $request->cfLastName,
            'username' => $request->cfUsername,
            'password' => md5($request->cfPassword),
            'gender' => $request->cfGender,
            'email' => $request->cfEmail,
            'phone' => $request->cfPhone,
            'country' => $request->cfCountry,
            'address' => $request->cfAddress,
            'enable_user' => true,

            'facebook' => $request->cfFacebook,
            'twitter' => $request->cfTwitter,
            'linkedin' => $request->cfLinkedin,
            'youtube' => $request->cfYouTube,

            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * Insert User Links
     *
     * @return array
     */
    public function insertUserLinks($id, $url)
    {
        UserLinks::insert([
            'id_user' => $id,
            'url_link' => $url,
            'enable_link' => true
        ]);
    }
}
