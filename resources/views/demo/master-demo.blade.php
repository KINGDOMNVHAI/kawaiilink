<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="{{asset('portfolio-demo/assets/vendors/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('portfolio-demo/assets/vendors/fontawesome/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('portfolio-demo/assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('portfolio-demo/assets/css/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('portfolio-demo/assets/css/kawaiilink.css')}}" rel="stylesheet">

    <title>{{$title}}</title>
</head>

<body>
    <div class="body_wrapper frm-vh-md-100">
        <div class="formify_body" data-bg-img="{{asset('portfolio-demo/assets/img/bg-danang.jpg')}}">
            <div class="overlay_bg" data-bg-color="rgba(0, 0, 0, 0.35)"></div>
            <!-- Nếu có hình ảnh thì sử dụng margin-top -->
            <div class="f_content" style="margin-top:500px;">
                <div class="container">
                    <div class="row">
                        <div class="formify_box formify_box_register background_transparent">
                            <div class="formify_header text-center">
                                <a href="#" class="mb-3 formify_logo"><img src="{{asset('portfolio-demo/assets/img/avatar-demo.png')}}" width="150px" alt=""></a>
                                <h4 class="form_title">
                                    {{$welcome}}<br>{{$fullname}}
                                </h4>
                                <h4 class="form_title">{{$slogan}}</h4>
                            </div>
                            <a href="https://www.youtube.com/channel/UCxt84pU0c9bPV-KLgViK2-g" title="" target="_blank" class="btn btn-social btn-google-red">
                                <i class="fab fa-youtube icon"></i> Your YouTube Link
                            </a>
                            <a href="#" title="" target="_blank" class="btn btn-social btn-facebook">
                                <i class="fab fa-facebook icon"></i> Your Facebook Link
                            </a>
                            <a href="#" title="" target="_blank" class="btn btn-social btn-twitter">
                                <i class="fab fa-twitter icon"></i> Your Twitter Link
                            </a>
                            <a href="https://www.linkedin.com/" title="" target="_blank" class="btn btn-social btn-linkedin">
                                <i class="fab fa-linkedin-in"></i> Your Linkedin Link
                            </a>
                            <a href="https://onlyfans.com/avaaddams/media" title="" target="_blank" class="btn btn-social btn-twitter">
                                <i class="fa-solid fa-fan"></i> Your OnlyFans Link
                            </a>
                            <div class="divider_border text-center">
                                <span class="or-text">OR</span>
                            </div>
                            <div class="link-box">
                                <a class="link-list" href="http://kdplayback.com/" title="" target="_blank" onclick="setLinkData(18340, 'nacchitabi')">
                                <p class="link-title">Your First Link</p></a>
                            </div>
                            <div class="link-box">
                                <a class="link-list" href="https://kingdomnvhai.info/" title="" target="_blank" onclick="setLinkData(18340, 'nacchitabi')">
                                <p class="link-title">Your Second Link</p></a>
                            </div>
                            <div class="link-box">
                                <a class="link-list" href="#" title="" target="_blank" onclick="setLinkData(18340, 'nacchitabi')">
                                <p class="link-title">Your Third Link</p></a>
                            </div>
                            <div class="link-box">
                                <a class="link-list" href="#" title="" target="_blank" onclick="setLinkData(18340, 'nacchitabi')">
                                <p class="link-title">Your Fourth Link</p></a>
                            </div>
                            <!-- <img src="{{asset('portfolio-demo/assets/img/bg-phone-1.jpg')}}" width="100%" alt=""> -->
                            {!! $embedcode !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->
    <script src="{{asset('portfolio-demo/assets/js/jquery-3.4.1.min.js')}}"></script>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="{{asset('portfolio-demo/assets/vendors/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{asset('portfolio-demo/assets/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('portfolio-demo/assets/js/main.js')}}"></script>
</body>
</html>
