@extends('multi-step.master-multi-step')

@section('content')

<div class="formify_right_fullwidth d-flex align-items-center justify-content-center">
    <div class="form_tab_two">
        <ul class="nav nav-tabs form_tab" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active show" id="One-tab" data-toggle="tab" href="#One" role="tab"
                    aria-controls="One" aria-selected="true">
                    <span>1</span>
                    Links
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="Two-tab" data-toggle="tab" href="#Two" role="tab"
                    aria-controls="Two" aria-selected="false">
                    <span>2</span>
                    Personal
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="Three-tab" data-toggle="tab" href="#Three" role="tab"
                    aria-controls="Three" aria-selected="false">
                    <span>3</span>
                    Experience
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="Four-tab" data-toggle="tab" href="#Four" role="tab"
                    aria-controls="Four" aria-selected="false">
                    <span>4</span>
                    Confirm
                </a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="One" role="tabpanel" aria-labelledby="One-tab">
                <div class="formify_box">
                    <h4 class="form_title">Insert Your <span>Links</span></h4>
                    <form action="#" class="signup_form row">
                        <div class="form-group col-md-6">
                            <label class="input_title" for="inputFacebook">Facebook</label>
                            <input type="text" class="form-control" id="inputFacebook" placeholder="Enter Facebook Link" oninput="confirmFacebook()">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="input_title" for="inputTwitter">Twitter</label>
                            <input type="text" class="form-control" id="inputTwitter" placeholder="Enter Twitter Link" oninput="confirmTwitter()">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="input_title" for="inputLinkedin">Linkedin</label>
                            <input type="text" class="form-control" id="inputLinkedin" placeholder="Enter Linkedin Link" oninput="confirmLinkedin()">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="input_title" for="inputYouTube">YouTube</label>
                            <input type="text" class="form-control" id="inputYouTube" placeholder="Enter YouTube Link" oninput="confirmYouTube()">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="input_title" for="inputLink1">Other Link 1</label>
                            <input type="text" class="form-control" id="inputLink1" placeholder="Enter Other Link 1" oninput="confirmLink1()">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="input_title" for="inputLink2">Other Link 2</label>
                            <input type="text" class="form-control" id="inputLink2" placeholder="Enter Other Link 2" oninput="confirmLink2()">
                        </div>
                        <div class="next_button text-right">
                            <button type="submit" class="btn thm_btn red_btn next_tab">Next to Your Personal<i class="arrow_right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane fade" id="Two" role="tabpanel" aria-labelledby="Two-tab">
                <div class="formify_box">
                    <h4 class="form_title">Tell us about your <span>Personal Details</span></h4>
                    <form action="#" class="signup_form row">
                        <div class="form-group col-md-6">
                            <label class="input_title" for="inputFirstName">First Name</label>
                            <input type="text" class="form-control" id="inputFirstName" name="inputFirstName"
                                placeholder="Enter First Name" required="" oninput="confirmFirstName()">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="input_title" for="inputLastName">Last Name</label>
                            <input type="text" class="form-control" id="inputLastName" name="inputLastName"
                                placeholder="Enter Last Name" required="" oninput="confirmLastName()">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="input_title" for="inputUsername">Username</label>
                            <input type="text" class="form-control" id="inputUsername" placeholder="Username" required="" oninput="confirmUsername()">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="input_title" for="inputEmail">Email address</label>
                            <input type="email" class="form-control" id="inputEmail" placeholder="Email address" required="" oninput="confirmEmail()">
                        </div>
                        <div class="form-group col-md-12">
                            <label class="input_title" for="inputPassword">Password (6 characters)</label>
                            <input type="password" class="form-control" id="inputPassword" placeholder="Password" required="" oninput="confirmPassword()">
                        </div>
                        <div class="form-group col-md-12">
                            <label class="input_title" for="inputConfirmPassword">Confirm Password <span id="resultCheckPassword" style="color:red"></span></label>
                            <input type="password" class="form-control" id="inputConfirmPassword" placeholder="Confirm Password" required="" oninput="checkPassword()">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="input_title" for="inputPhone">Phone number</label>
                            <input type="text" class="form-control" id="inputPhone" placeholder="97501456" required="" oninput="confirmPhone()">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="input_title">Gender</label>
                            <select class="niceselect" id="inputGender" name="inputGender" onchange="confirmGender();">
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                                <option value="other">Other</option>
                            </select>
                        </div>
                        <!-- <div class="form-group col-lg-12">
                            <label class="input_title" for="inputPhone2">Description</label>
                            <textarea name="message" id="message" cols="30" rows="10" placeholder="Write your job experience"></textarea>
                        </div> -->
                    </form>
                    <div class="next_button d-flex align-items-center justify-content-between">
                        <a href="#" class="prev_tab"><i class="arrow_left"></i> Back</a>
                        <button type="submit" class="btn thm_btn red_btn next_tab">Next to Your Experience <i class="arrow_right"></i></button>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="Three" role="tabpanel" aria-labelledby="Three-tab">
                <div class="formify_box">
                    <h4 class="form_title">Tell us about your <span>Experience Details</span></h4>
                    <p><b>(If you don't need a job, you can do the typing later)</b></p>
                    <form action="#" class="signup_form row">
                        <div class="form-group col-lg-6">
                            <label class="input_title" for="inputJob">Job Title</label>
                            <input type="text" class="form-control" id="inputJob" placeholder="Job Title">
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="input_title">Number of year</label>
                            <select class="niceselect">
                                <option value="1 - 2 year">1 - 2 year</option>
                                <option value="3 - 4 years">3 - 4 years</option>
                                <option value="More 4 years">More 4 years</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="input_title">Employment type</label>
                            <select class="niceselect">
                                <option value="fulltime">Full time</option>
                                <option value="parttime">Part time</option>
                                <option value="selfemployed">Self Employed</option>
                                <option value="freelancer">Freelancer</option>
                                <option value="contract">Contract</option>
                                <option value="internship">Internship</option>
                                <option value="apprenticeship">Apprenticeship</option>
                                <option value="seasonal">Seasonal</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="input_title" for="inputCountry">Country (Living, Studying or Working)</label>
                            <input type="text" class="form-control" id="inputCountry" placeholder="Japan, Vietnam" required="" oninput="confirmCountry()">
                        </div>
                        <div class="form-group col-lg-12">
                            <label class="input_title" for="inputAddress">Address (Living, Studying or Working)</label>
                            <input type="text" class="form-control" id="inputAddress"
                                placeholder="PO Box 16122 Collins Street West Victoria" required="" oninput="confirmAddress()">
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="input_title" for="inputSchool">School</label>
                            <input type="text" class="form-control" id="inputSchool" placeholder="School Name">
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="input_title" for="inputName6">Degree</label>
                            <select class="niceselect">
                                <option value="japan">College</option>
                                <option value="vietnam">University</option>
                            </select>
                        </div>
                    </form>
                    <div class="next_button d-flex align-items-center justify-content-between">
                        <a href="#" class="prev_tab"><i class="arrow_left"></i> Back</a>
                        <button type="submit" class="btn thm_btn red_btn next_tab">Next to Your Confirm <i class="arrow_right"></i></button>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="Four" role="tabpanel" aria-labelledby="Four-tab">
                <div class="formify_box">
                    <h4 class="form_title">Tell us about your <span>Confirm Details</span></h4>
                    <p><b>Confirm your information to sign up</b></p>
                    <form action="{{route('register-insert')}}" method="POST" class="signup_form row">
                        {!! csrf_field() !!}
                        <div class="form-group col-md-6">
                            <p>Facebook: <span id="cfFacebookHTML"></span></p>
                            <p>Twitter: <span id="cfTwitterHTML"></span></p>
                            <p>Linkedin: <span id="cfLinkedinHTML"></span></p>
                            <p>YouTube: <span id="cfYouTubeHTML"></span></p>
                            <p>Other Link 1: <span id="cfLink1HTML"></span></p>
                            <p>Other Link 2: <span id="cfLink2HTML"></span></p>
                            <input type="hidden" id="cfFacebook" name="cfFacebook" disabled>
                            <input type="hidden" id="cfTwitter" name="cfTwitter" disabled>
                            <input type="hidden" id="cfLinkedin" name="cfLinkedin" disabled>
                            <input type="hidden" id="cfYouTube" name="cfYouTube" disabled>
                            <input type="hidden" id="cfLink1" name="cfLink1" disabled>
                            <input type="hidden" id="cfLink2" name="cfLink2" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <p>First Name: <span id="cfFirstNameHTML"></span></p>
                            <p>Last Name: <span id="cfLastNameHTML"></span></p>
                            <p>Username: <span id="cfUsernameHTML"></span></p>
                            <p>Password: <span id="cfPasswordHTML"></span></p>
                            <p>Gender: <span id="cfGenderHTML">male</span></p>
                            <p>Email: <span id="cfEmailHTML"></span></p>
                            <p>Phone: <span id="cfPhoneHTML"></span></p>
                            <p>Country: <span id="cfCountryHTML"></span></p>
                            <p>Address: <span id="cfAddressHTML"></span></p>
                            <input type="hidden" id="cfFirstName" name="cfFirstName" disabled>
                            <input type="hidden" id="cfLastName" name="cfLastName" disabled>
                            <input type="hidden" id="cfUsername" name="cfUsername" disabled>
                            <input type="hidden" id="cfPassword" name="cfPassword" disabled>
                            <input type="hidden" id="cfCheckPassword" name="cfCheckPassword" disabled>
                            <input type="hidden" id="cfGender" name="cfGender" value="male" disabled>
                            <input type="hidden" id="cfEmail" name="cfEmail" disabled>
                            <input type="hidden" id="cfPhone" name="cfPhone" disabled>
                            <input type="hidden" id="cfCountry" name="cfCountry" value="" disabled>
                            <input type="hidden" id="cfAddress" name="cfAddress" disabled>
                        </div>
                        <div class="form-group col-lg-12 d-flex align-items-center justify-content-between">
                            <a href="#" class="prev_tab"><i class="arrow_left"></i> Back</a>
                            <button type="submit" class="btn thm_btn red_btn">Submit Your Application</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function checkPassword() {
    var pass = document.getElementById("inputPassword").value;
    var confirm = document.getElementById("inputConfirmPassword").value;
    if (pass != confirm) {
        document.getElementById("resultCheckPassword").innerHTML = "Not Matching";
        document.getElementById("cfPasswordHTML").innerHTML = "Not Matching";
    } else {
        document.getElementById("resultCheckPassword").innerHTML = "Matching";
        document.getElementById("cfPasswordHTML").innerHTML = "Matching";
    }
}
function confirmFacebook() {
    var x = document.getElementById("inputFacebook").value;
    document.getElementById("cfFacebook").value = x;
    document.getElementById("cfFacebookHTML").innerHTML = x;
}
function confirmTwitter() {
    var x = document.getElementById("inputTwitter").value;
    document.getElementById("cfTwitter").value = x;
    document.getElementById("cfTwitterHTML").innerHTML = x;
}
function confirmLinkedin() {
    var x = document.getElementById("inputLinkedin").value;
    document.getElementById("cfLinkedin").value = x;
    document.getElementById("cfLinkedinHTML").innerHTML = x;
}
function confirmYouTube() {
    var x = document.getElementById("inputYouTube").value;
    document.getElementById("cfYouTube").value = x;
    document.getElementById("cfYouTubeHTML").innerHTML = x;
}
function confirmLink1() {
    var x = document.getElementById("inputLink1").value;
    document.getElementById("cfLink1").value = x;
    document.getElementById("cfLink1HTML").innerHTML = x;
}
function confirmLink2() {
    var x = document.getElementById("inputLink2").value;
    document.getElementById("cfLink2").value = x;
    document.getElementById("cfLink2HTML").innerHTML = x;
}
function confirmFirstName() {
    var x = document.getElementById("inputFirstName").value;
    document.getElementById("cfFirstName").value = x;
    document.getElementById("cfFirstNameHTML").innerHTML = x;
}
function confirmLastName() {
    var x = document.getElementById("inputLastName").value;
    document.getElementById("cfLastName").value = x;
    document.getElementById("cfLastNameHTML").innerHTML = x;
}
function confirmGender() {
    var x = document.getElementById("inputGender").value;
    document.getElementById("cfGender").value = x;
    document.getElementById("cfGenderHTML").innerHTML = x;
}
function confirmEmail() {
    var x = document.getElementById("inputEmail").value;
    document.getElementById("cfEmail").value = x;
    document.getElementById("cfEmailHTML").innerHTML = x;
}
function confirmPhone() {
    var x = document.getElementById("inputPhone").value;
    document.getElementById("cfPhone").value = x;
    document.getElementById("cfPhoneHTML").innerHTML = x;
}
function confirmCountry() {
    var x = document.getElementById("inputCountry").value;
    document.getElementById("cfCountry").value = x;
    document.getElementById("cfCountryHTML").innerHTML = x;
}
function confirmAddress() {
    var x = document.getElementById("inputAddress").value;
    document.getElementById("cfAddress").value = x;
    document.getElementById("cfAddressHTML").innerHTML = x;
}
function confirmUsername() {
    var x = document.getElementById("inputUsername").value;
    document.getElementById("cfUsername").value = x;
    document.getElementById("cfUsernameHTML").innerHTML = x;
}
function confirmPassword() {
    var pass = document.getElementById("inputPassword").value;
    document.getElementById("cfPassword").value = pass;
}
</script>

@endsection
