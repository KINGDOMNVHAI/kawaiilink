<!doctype html>
<html lang="en">
<head>
    @include('multi-step.block.head')
</head>

<body>
    <div class="body_wrapper frm-vh-md-100">
        <div class="formify_body formify_signup_fullwidth formify_signup_fullwidth_two d-flex">
            <div class="formify_left_fullwidth formify_left_top_logo frm-vh-md-100 d-flex align-items-center justify-content-center"
                data-bg-color="#FFEFF9">
                <a href="index.html" class="top_logo"><img src="{{asset('multistep/assets/img/logo.png')}}" alt=""></a>
                <img class="img-fluid" src="{{asset('multistep/assets/img/personal_img.png')}}" alt="">
            </div>
            @yield('content')
        </div>
    </div>
    @include('multi-step.block.footer')
</body>
</html>
