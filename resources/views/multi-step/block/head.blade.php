<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link href="{{asset('multistep/assets/vendors/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('multistep/assets/vendors/nice-select/css/nice-select.css')}}" rel="stylesheet">
<link href="{{asset('multistep/assets/vendors/elagent-icon/style.css')}}" rel="stylesheet">
<link href="{{asset('multistep/assets/css/style.css')}}" rel="stylesheet">
<link href="{{asset('multistep/assets/css/responsive.css')}}" rel="stylesheet">

<title>Formify</title>
