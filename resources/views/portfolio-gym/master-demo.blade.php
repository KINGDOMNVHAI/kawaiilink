<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="{{asset('portfolio-gym/assets/vendors/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('portfolio-gym/assets/vendors/fontawesome/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('portfolio-gym/assets/css/kawaiilink.css')}}" rel="stylesheet">
    <title>{{$title}}</title>
</head>

<body>
<!-- Section: Design Block -->
<section class="text-center text-lg-start">
    <style>
    .cascading-right {
        margin-right: -50px;
    }

    @media (max-width: 991.98px) {
        .cascading-right {margin-right: 0;}
    }
    </style>

    <!-- Jumbotron -->
    <div class="container py-4">
        <div class="row g-0 align-items-center">
            <div class="col-lg-6 mb-5 mb-lg-0">
                <div class="card cascading-right" style="background: hsla(0, 0%, 100%, 0.55); backdrop-filter: blur(30px);">
                    <div class="card-body p-5 shadow-5 text-center">
                        <h2 class="fw-bold mb-5">{{$welcome}} {{$fullname}}</h2>
                        <img src="{{asset('portfolio-gym/assets/img/avatar-demo.png')}}" class="mb-4" width="150px" alt="{{$fullname}}">
                        <p>{{$slogan}}</p>

                        <div class="text-center"></div>
                        <!-- 2 column grid layout with text inputs for the first and last names -->
                        <div class="row">
                            <div class="col-md-6 mb-4">
                                <div class="form-outline">
                                <a href="#" class="btn btn-danger btn-block mb-4"><i class="fab fa-youtube"></i> Your YouTube Channel</a>
                                </div>
                                <div class="form-outline">
                                <a href="#" class="btn btn-twitter btn-block mb-4"><i class="fab fa-twitter"></i> Your Twitter Link</a>
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="form-outline">
                                <a href="#" class="btn btn-primary btn-block mb-4"><i class="fab fa-facebook-f"></i> Your Facebook Link</a>
                                </div>
                                <div class="form-outline">
                                <a href="#" class="btn btn-linkedin btn-block mb-4"><i class="fab fa-linkedin-in"></i> Your Linkedin Link</a>
                                </div>
                            </div>
                        </div>

                        <a href="#" class="btn btn-primary btn-block mb-4">Your First Link</a>
                        <a href="#" class="btn btn-secondary btn-block mb-4">Your Second Link</a>
                        <a href="#" class="btn btn-primary btn-block mb-4">Your Third Link</a>
                        <a href="#" class="btn btn-secondary btn-block mb-4">Your Fourth Link</a>

                        {!! $embedcode !!}
                    </div>
                </div>
            </div>

            <div class="col-lg-6 mb-5 mb-lg-0">
                <img src="{{asset('portfolio-gym/assets/img/pretty-boxer-rainy-day-jordan-seductively-poses-in-gym-after-training-67926396-003_8689.jpg')}}" class="w-100 rounded-4 shadow-4" alt="" />
            </div>
        </div>
    </div>
    <!-- Jumbotron -->
</section>
<!-- Section: Design Block -->
</body>
</html>
