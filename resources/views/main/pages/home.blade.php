@extends('main.master-main')

@section('content-navbar')

<!-- Hero image -->
<div id="main-hero" class="hero-body">
    <div class="container">
        <div class="columns is-vcentered">
            <div class="column is-5 is-offset-1">
                <!-- Header Form -->
                <div class="hero-form">
                    <div class="flex-card">
                        <form action="{{route('demo-portfolio-get')}}" method="GET">
                        {!! csrf_field() !!}
                        <p>Hãy nhập thông tin để đăng ký</p>
                        <div class="field">
                            <label>Full Name <small>*</small></label>
                            <div class="control has-icons-left">
                                <input class="input is-medium has-shadow" type="text" name="fullname" placeholder="君の名は" />
                                <span class="icon is-small is-left"><i class="sl sl-icon-user"></i></span>
                            </div>
                        </div>

                        <div class="field">
                            <label>Country <small>*</small></label>
                            <div class="control has-icons-left">
                                <input class="input is-medium has-shadow" type="text" name="country" placeholder="日本" />
                                <span class="icon is-small is-left"><i class="sl sl-icon-location"></i></span>
                            </div>
                        </div>

                        <div class="field">
                            <label>Email Address <small>*</small></label>
                            <div class="control has-icons-left">
                                <input class="input is-medium has-shadow" type="text" name="email" placeholder="メールアドレス kawaiilink@gmail.com" />
                                <span class="icon is-small is-left"><i class="sl sl-icon-envelope-open"></i></span>
                            </div>
                        </div>

                        <div class="field">
                            <label>Phone <small>*</small></label>
                            <div class="control has-icons-left">
                                <input class="input is-medium has-shadow" type="text" name="phone" placeholder="0123xxx" />
                                <span class="icon is-small is-left"><i class="sl sl-icon-phone"></i></span>
                            </div>
                        </div>

                        <!-- <div class="field">
                            <label>Password <small>*</small></label>
                            <div class="control has-icons-left">
                                <input class="input is-medium has-shadow" type="password" placeholder="" />
                                <span class="icon is-small is-left"><i class="sl sl-icon-lock"></i></span>
                            </div>
                        </div> -->

                        <!-- <div class="field switch-toggle-wrap is-grouped pt-10 pb-10">
                            <div class="control">
                                <label class="slide-toggle">
                                    <input type="checkbox" />
                                    <span class="toggler">
                                        <span class="active"><i data-feather="check"></i></span>
                                        <span class="inactive"><i data-feather="circle"></i></span>
                                    </span>
                                </label>
                            </div>
                            <div class="control">
                                <span>By signing up, I agree to the Kawaiilink<a>Terms and Conditions</a>.</span>
                            </div>
                        </div> -->

                        <div class="field">
                            <div class="control">
                                <button class="button button-cta success-btn is-bold is-fullwidth raised" type="submit">
                                    {{ __('main-home.visit_demo_no_register') }}
                                </button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="column is-5 signup-column has-text-left">
                <h1 class="title main-title text-bold is-2">Make a beauty (kawaii) profile</h1>
                <h2 class="subtitle is-5 is-light no-margin-bottom">
                    {{ __('main-home.take_control_over_your_profile_and_portfolio') }}
                </h2>

                <div class="buttons mt-20">
                    <a href="{{route('main-register')}}" class="button button-cta btn-align is-bold primary-btn">
                        {{ __('main-home.sign_up') }}
                    </a>
                    <!-- <a href="" class="button button-cta btn-align is-bold light-btn"> Get a Quote </a> -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('content')

<!-- Product -->
<div id="product" class="section is-medium is-skewed-sm">
    <div class="container is-reverse-skewed-sm">
        <!-- Title -->
        <div class="section-title-wrapper has-text-centered">
            <div class="bg-number">1</div>
            <h2 class="section-title-landing">{{ __('main-home.make_profile_and_list_url_just_got_simple') }}</h2>
            <h4>{{ __('main-home.learn_to_express_yourself_better_and_faster') }}</h4>
        </div>

        <div class="content-wrapper">
            <!-- Icon boxes -->
            <div class="columns is-vcentered">
                <div class="column is-5 is-offset-1 has-text-centered">
                    <div class="columns is-vcentered has-text-centered is-multiline">
                        <!-- Icon box -->
                        <div class="column is-6">
                            <div class="flex-card icon-card hover-inset">
                                <img src="{{asset('main/assets/img/graphics/icons/records-core.svg')}}" data-base-url="assets/img/graphics/icons/records" data-extension=".svg" alt="" />
                                <div class="icon-card-text is-clean">{{ __('main-home.profile') }}</div>
                            </div>
                        </div>
                        <!-- Icon box -->
                        <div class="column is-6">
                            <div class="flex-card icon-card hover-inset">
                                <img src="{{asset('main/assets/img/graphics/icons/invoice-core.svg')}}" data-base-url="assets/img/graphics/icons/invoice" data-extension=".svg" alt="" />
                                <div class="icon-card-text is-clean">{{ __('main-home.free') }}</div>
                            </div>
                        </div>
                        <!-- Icon box -->
                        <div class="column is-6">
                            <div class="flex-card icon-card hover-inset">
                                <img src="{{asset('main/assets/img/graphics/icons/study-core.svg')}}" data-base-url="assets/img/graphics/icons/study-core" data-extension=".svg" alt="" />
                                <div class="icon-card-text is-clean">{{ __('main-home.simple') }}</div>
                            </div>
                        </div>
                        <!-- Icon box -->
                        <div class="column is-6">
                            <div class="flex-card icon-card hover-inset">
                                <img src="{{asset('main/assets/img/graphics/icons/sync-core.svg')}}" data-base-url="assets/img/graphics/icons/sync" data-extension=".svg" alt="" />
                                <div class="icon-card-text is-clean">{{ __('main-home.cloud_sync') }}</div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Feature text -->
                <div class="column is-5">
                    <div class="side-feature-text">
                        <h2 class="feature-headline is-clean">
                            {{ __('main-home.free_design_for_free_idea') }}
                        </h2>
                        <p class="body-color">
                            {{ __('main-home.free_design_for_free_idea_present') }}
                        </p>
                        <div class="button-wrap">
                            <a href="{{route('main-register')}}" class="button button-cta btn-align raised primary-btn">Try it free</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Services -->
<div id="services" class="section section-feature-grey is-medium is-skewed-sm">
    <div class="container is-reverse-skewed-sm">
        <!-- Title -->
        <div class="section-title-wrapper has-text-centered">
            <div class="bg-number">2</div>
            <h2 class="section-title-landing">{{ __('main-home.grow_your_personal_branding') }}</h2>
            <h4>{{ __('main-home.and_start_making_money') }}</h4>
        </div>

        <div class="content-wrapper hover-cards">
            <!-- Hover boxes -->
            <div class="columns is-vcentered">
                <div class="column is-10 is-offset-1">
                    <div class="columns is-vcentered">
                        <div class="column is-6">
                            <!-- Hover box -->
                            <div class="flex-card icon-card-hover first-card light-bordered">
                                <h3 class="card-title is-clean">Flexible online profile builder</h3>
                                <p class="card-description">
                                    Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no,
                                    has eu lorem convenire incorrupte.
                                </p>
                            </div>
                            <!-- Hover box -->
                            <div class="flex-card icon-card-hover second-card light-bordered">
                                <h3 class="card-title is-clean">Multi URL for your visitor</h3>
                                <p class="card-description">
                                    Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no,
                                    has eu lorem convenire incorrupte.
                                </p>
                            </div>
                        </div>
                        <div class="column is-6">
                            <!-- Hover box -->
                            <div class="flex-card icon-card-hover third-card light-bordered">
                                <h3 class="card-title is-clean">Setup workflows</h3>
                                <p class="card-description">
                                    Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no,
                                    has eu lorem convenire incorrupte.
                                </p>
                            </div>
                            <!-- Hover box -->
                            <div class="flex-card icon-card-hover fourth-card light-bordered">
                                <h3 class="card-title is-clean">Attractive profile templates</h3>
                                <p class="card-description">
                                    Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no,
                                    has eu lorem convenire incorrupte.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-wrap has-text-centered is-title-reveal">
                <a href="kit2-signup.html" class="button button-cta btn-align raised primary-btn raised">
                    Start your Free Trial
                </a>
            </div>
        </div>
    </div>
</div>

<!-- Slanted feature Section -->
<section class="section section-primary is-medium is-skewed-sm is-relative">
    <div class="container slanted-container is-reverse-skewed-sm">
        <div class="columns is-vcentered">
            <!-- Content -->
            <div class="column is-5">
                <div class="side-feature-text">
                    <h2 class="title is-3 clean-text light-text">
                        Get started in a breeze
                    </h2>
                    <p class="light-text">
                        Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu
                        lorem convenire incorrupte. Vis mutat altera percipit ad, ipsum
                        prompta ius eu. Sanctus appellantur vim ea. Dolorem delicata vis te,
                        aperiam nostrum ut per.
                    </p>
                    <div class="button-wrap">
                        <a href="kit2-signup.html" class="button button-cta light-btn btn-outlined is-bold">
                            Get started
                        </a>
                    </div>
                </div>
            </div>
            <!-- Featured image -->
            <div class="column is-6 is-offset-1">
                <img class="featured-svg" src="{{asset('main/assets/img/graphics/widgets/mini-widget-4-core.png')}}" data-base-url="assets/img/graphics/widgets/mini-widget-4" data-extension=".png" alt="" />
            </div>
        </div>
    </div>
</section>

@endsection
