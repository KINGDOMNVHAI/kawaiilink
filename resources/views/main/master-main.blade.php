
<!DOCTYPE html>
<html lang="en">
<head>
    @include('main.block.head')
</head>

<body class="is-theme-core">
    <div class="pageloader"></div>
    <div class="infraloader is-active"></div>

    @include('main.block.navbar')

    @yield('content')

    <!-- Testimonials section -->
    <section id="card-testimonials" class="section section-feature-grey is-medium is-skewed-sm">
        <div class="container is-reverse-skewed-sm">
            <!-- Title -->
            <div class="section-title-wrapper has-text-centered">
                <div class="bg-number">3</div>
                <h2 class="section-title-landing">We are Trusted</h2>
                <h4>Access integrations and new features in a matter of seconds</h4>
            </div>

            <div class="content-wrapper">
                <!-- Testimonials -->
                <div class="columns">
                    <div class="column is-10 is-offset-1">
                        <div class="columns is-vcentered">
                            <div class="column is-6">
                                <!-- Testimonial item -->
                                <div class="flex-card testimonial-card light-raised light-bordered padding-20">
                                    <div class="testimonial-title">Amazed by the product</div>
                                    <div class="testimonial-text">
                                        Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no,
                                        has eu lorem convenire incorrupte. Vis mutat altera percipit
                                        ad.
                                    </div>
                                    <div class="user-id">
                                        <img src="https://via.placeholder.com/250x250" alt="" data-demo-src="{{asset('main/assets/img/avatars/dan.png')}}" />
                                        <div class="info">
                                            <div class="name clean-text">Dan Shwartz</div>
                                            <div class="position">Software engineer</div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Testimonial item -->
                                <div class="flex-card testimonial-card light-raised light-bordered padding-20">
                                    <div class="testimonial-title">My tasks are now painless</div>
                                    <div class="testimonial-text">
                                        Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no,
                                        has eu lorem convenire incorrupte. Vis mutat altera percipit
                                        ad.
                                    </div>
                                    <div class="user-id">
                                        <img src="https://via.placeholder.com/250x250" alt="" data-demo-src="{{asset('main/assets/img/avatars/janet.jpg')}}" />
                                        <div class="info">
                                            <div class="name clean-text">Jane Guzmann</div>
                                            <div class="position">CFO</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column is-6">
                                <!-- Testimonial item -->
                                <div class="flex-card testimonial-card light-raised light-bordered padding-20">
                                    <div class="testimonial-title">Very nice support</div>
                                    <div class="testimonial-text">
                                        Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no,
                                        has eu lorem convenire incorrupte. Vis mutat altera percipit
                                        ad.
                                    </div>
                                    <div class="user-id">
                                        <img src="https://via.placeholder.com/250x250" alt="" data-demo-src="{{asset('main/assets/img/avatars/helen.jpg')}}" />
                                        <div class="info">
                                            <div class="name clean-text">Hellen Miller</div>
                                            <div class="position">Accountant</div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Testimonial item -->
                                <div class="
                      flex-card
                      testimonial-card
                      light-raised light-bordered
                      padding-20
                    ">
                                    <div class="testimonial-title">My income has doubled</div>
                                    <div class="testimonial-text">
                                        Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no,
                                        has eu lorem convenire incorrupte. Vis mutat altera percipit
                                        ad.
                                    </div>
                                    <div class="user-id">
                                        <img src="https://via.placeholder.com/250x250" alt="" data-demo-src="assets/img/avatars/anthony.jpg" />
                                        <div class="info">
                                            <div class="name clean-text">Anthony Leblanc</div>
                                            <div class="position">Founder at Hereby</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Clients grid -->
                <div class="grid-clients three-grid">
                    <div class="columns is-vcentered">
                        <div class="column"></div>
                        <div class="column">
                            <a><img class="client" src="assets/img/logos/custom/systek.svg" alt="" /></a>
                        </div>
                        <div class="column">
                            <a><img class="client" src="assets/img/logos/custom/phasekit.svg" alt="" /></a>
                        </div>
                        <div class="column">
                            <a><img class="client" src="assets/img/logos/custom/grubspot.svg" alt="" /></a>
                        </div>
                        <div class="column"></div>
                    </div>
                    <div class="columns is-vcentered is-separator">
                        <div class="column"></div>
                        <div class="column">
                            <a><img class="client" src="assets/img/logos/custom/tribe.svg" alt="" /></a>
                        </div>
                        <div class="column">
                            <a><img class="client" src="assets/img/logos/custom/kromo.svg" alt="" /></a>
                        </div>
                        <div class="column">
                            <a><img class="client" src="assets/img/logos/custom/covenant.svg" alt="" /></a>
                        </div>
                        <div class="column"></div>
                    </div>
                    <div class="columns is-vcentered is-separator">
                        <div class="column"></div>
                        <div class="column">
                            <a><img class="client" src="assets/img/logos/custom/infinite.svg" alt="" /></a>
                        </div>
                        <div class="column">
                            <a><img class="client" src="assets/img/logos/custom/gutwork.svg" alt="" /></a>
                        </div>
                        <div class="column">
                            <a><img class="client" src="assets/img/logos/custom/proactive.svg" alt="" /></a>
                        </div>
                        <div class="column"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('main.block.footer')

    <!-- Side navigation -->
    <div class="side-navigation-menu">
        <!-- Categories menu -->
        <div class="category-menu-wrapper">
            <!-- Menu -->
            <ul class="categories">
                <li class="square-logo"><img src="{{asset('main/assets/img/logos/square-white.svg')}}" alt=""></li>
                <li class="category-link is-active" data-navigation-menu="demo-pages"><i class="sl sl-icon-layers"></i></li>
                <li class="category-link" data-navigation-menu="onepagers"><i class="sl sl-icon-docs"></i></li>
                <li class="category-link" data-navigation-menu="components"><i class="sl sl-icon-grid"></i></li>
            </ul>
            <!-- Menu -->

            <ul class="author">
                <li>
                    <!-- Theme author -->
                    <a href="https://cssninja.io/" target="_blank">
                        <img class="main-menu-author" src="{{asset('main/assets/img/logos/cssninja.svg')}}" alt="">
                    </a>
                </li>
            </ul>
        </div>
        <!-- /Categories menu -->

        <!-- Navigation menu -->
        <div id="demo-pages" class="navigation-menu-wrapper animated preFadeInRight fadeInRight">
            <!-- Navigation Header -->
            <div class="navigation-menu-header">
                <span>Demo pages</span>
                <a class="ml-auto hamburger-btn navigation-close" href="javascript:void(0);">
                    <span class="menu-toggle">
                        <span class="icon-box-toggle">
                            <span class="rotate">
                                <i class="icon-line-top"></i>
                                <i class="icon-line-center"></i>
                                <i class="icon-line-bottom"></i>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
            <!-- Navigation Body -->
            <ul class="navigation-menu">
                <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">weekend</span>Agency</a>
                    <ul>
                        <li><a class="is-submenu" href="agency-home.html">Homepage</a></li>
                        <li><a class="is-submenu" href="agency-about.html">About</a></li>
                        <li><a class="is-submenu" href="agency-portfolio.html">Portfolio</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">wb_incandescent</span>Startup</a>
                    <ul>
                        <li><a class="is-submenu" href="startup-landing.html">Homepage</a></li>
                        <li><a class="is-submenu" href="startup-about.html">About</a></li>
                        <li><a class="is-submenu" href="startup-product.html">Product</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">apps</span>Software</a>
                    <ul>
                        <li><a class="is-submenu" href="kit15-landing-5.html">Conference v1</a></li>
                        <li><a class="is-submenu" href="kit15-landing-6.html">Conference v2</a></li>
                        <li><a class="is-submenu" href="kit1-landing.html">Projects v1</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">timer</span>Freelancer</a>
                    <ul>
                        <li><a class="is-submenu" href="kit2-landing-9.html">Freelancer v1</a></li>
                        <li><a class="is-submenu" href="kit2-landing.html">Freelancer v2</a></li>
                        <li><a class="is-submenu" href="kit2-landing-2.html">Freelancer v3</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">voice_chat</span>Videoconference</a>
                    <ul>
                        <li><a class="is-submenu" href="kit16-landing.html">Videoconference v1</a></li>
                        <li><a class="is-submenu" href="kit16-landing-2.html">Videoconference v2</a></li>
                        <li><a class="is-submenu" href="kit16-landing-3.html">Videoconference v3</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">domain</span>Business</a>
                    <ul>
                        <li><a class="is-submenu" href="kit17-landing.html">Crypto v1</a></li>
                        <li><a class="is-submenu" href="kit17-landing-2.html">Crypto v2</a></li>
                        <li><a class="is-submenu" href="kit17-landing-3.html">Crypto v3</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">shopping_cart</span>Ecommerce</a>
                    <ul>
                        <li><a class="is-submenu" href="commerce-home.html">Shop page</a></li>
                        <li><a class="is-submenu" href="commerce-product-landing.html">Landing page</a></li>
                        <li><a class="is-submenu" href="commerce-product-landing-2.html">Landing page</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">mouse</span>Services</a>
                    <ul>
                        <li><a class="is-submenu" href="kit12-landing.html">Consulting v1</a></li>
                        <li><a class="is-submenu" href="kit12-landing-2.html">Consulting v2</a></li>
                        <li><a class="is-submenu" href="kit12-landing-3.html">Consulting v3</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">work</span>Jobs</a>
                    <ul>
                        <li><a class="is-submenu" href="kit13-landing.html">Jobs v1</a></li>
                        <li><a class="is-submenu" href="kit13-landing-2.html">Jobs v2</a></li>
                        <li><a class="is-submenu" href="kit8-landing.html">Work v1</a></li>
                    </ul>
                </li>
                <li class="has-children">
                    <a class="parent-link" href="#">
                        <span class="material-icons">people</span>Customers</a>
                    <ul>
                        <li><a class="is-submenu" href="kit7-landing.html">CRM v1</a></li>
                        <li><a class="is-submenu" href="kit7-landing-alt.html">CRM v2</a></li>
                        <li><a class="is-submenu" href="kit7-landing-3.html">CRM v3</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- Navigation menu -->
        <div id="onepagers" class="navigation-menu-wrapper animated preFadeInRight fadeInRight is-hidden">
            <!-- Navigation Header -->
            <div class="navigation-menu-header">
                <span>Sub Pages</span>
                <a class="ml-auto hamburger-btn navigation-close" href="javascript:void(0);">
                    <span class="menu-toggle">
                        <span class="icon-box-toggle">
                            <span class="rotate">
                                <i class="icon-line-top"></i>
                                <i class="icon-line-center"></i>
                                <i class="icon-line-bottom"></i>
                            </span>
                    </span>
                    </span>
                </a>
            </div>
            <!-- Navigation body -->
            <ul class="navigation-menu">
                <li class="has-children"><a class="parent-link has-new" href="#"><span class="material-icons">account_circle</span>Personal</a>
                    <ul>
                        <li><a class="is-submenu" href="profile-1.html">Profile v1</a></li>
                        <li><a class="is-submenu" href="profile-2.html">Profile v2</a></li>
                        <li><a class="is-submenu" href="profile-3.html">Profile v3</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link has-new" href="#"><span class="material-icons">shopping_cart</span>Commerce</a>
                    <ul>
                        <li><a class="is-submenu" href="commerce-home.html">Shop Home</a></li>
                        <li><a class="is-submenu" href="commerce-product-landing.html">Product landing v1</a></li>
                        <li><a class="is-submenu" href="commerce-product-landing-2.html">Product landing v2</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link has-new" href="#"><span class="material-icons">domain</span>Company</a>
                    <ul>
                        <li><a class="is-submenu" href="about-page-1.html">About v1</a></li>
                        <li><a class="is-submenu" href="about-page-2.html">About v2</a></li>
                        <li><a class="is-submenu" href="case-study-1.html">Case Study v1</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link has-new" href="#"><span class="material-icons">call</span>Contact</a>
                    <ul>
                        <li><a class="is-submenu" href="contact-page-1.html">Contact v1</a></li>
                        <li><a class="is-submenu" href="contact-page-2.html">Contact v2</a></li>
                        <li><a class="is-submenu" href="contact-page-3.html">Contact v3</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link has-new" href="#"><span class="material-icons">book</span>Blog</a>
                    <ul>
                        <li><a class="is-submenu" href="blog-posts-full.html">Posts List v1</a></li>
                        <li><a class="is-submenu" href="blog-posts-full-alt.html">Posts List v2</a></li>
                        <li><a class="is-submenu" href="blog-posts-side.html">Posts List v3</a></li>
                    </ul>
                </li>
                <li class="has-children">
                    <a class="parent-link has-new" href="#"><span class="material-icons">verified_user</span>Authentication</a>
                    <ul>
                        <li><a class="is-submenu" href="startup-login.html">Login v1</a></li>
                        <li><a class="is-submenu" href="startup-login-2.html">Login v2</a></li>
                        <li><a class="is-submenu" href="kit1-login.html">Login v3</a></li>
                    </ul>
                </li>
                <li class="has-children">
                    <a class="parent-link has-new" href="#"><span class="material-icons">highlight</span>Error Pages</a>
                    <ul>
                        <li><a class="is-submenu" href="error-1.html">Error v1</a></li>
                        <li><a class="is-submenu" href="error-2.html">Error v2</a></li>
                        <li><a class="is-submenu" href="error-3.html">Error v3</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- Navigation menu -->
        <div id="components" class="navigation-menu-wrapper animated preFadeInRight fadeInRight is-hidden">
            <!-- Navigation Header -->
            <div class="navigation-menu-header">
                <span>Components</span>
                <a class="ml-auto hamburger-btn navigation-close" href="javascript:void(0);">
                    <span class="menu-toggle">
                        <span class="icon-box-toggle">
                            <span class="rotate">
                                <i class="icon-line-top"></i>
                                <i class="icon-line-center"></i>
                                <i class="icon-line-bottom"></i>
                            </span>
                    </span>
                    </span>
                </a>
            </div>
            <!-- Navigation body -->
            <ul class="navigation-menu">
                <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">view_quilt</span>Layout</a>
                    <ul>
                        <li><a class="is-submenu" href="_components-layout-grid.html">Grid system</a></li>
                        <li><a class="is-submenu" href="_components-layout-parallax.html">Parallax</a></li>
                        <li><a class="is-submenu" href="_components-layout-headers.html">Headers</a></li>
                        <li><a class="is-submenu" href="_components-layout-footers.html">Footers</a></li>
                        <li><a class="is-submenu" href="_components-layout-typography.html">Typography</a></li>
                        <li><a class="is-submenu" href="_components-layout-colors.html">Colors</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">subject</span>Navbars</a>
                    <ul>
                        <li><a class="is-submenu" href="_components-layout-navbar-fade-light.html">Fade light</a></li>
                        <li><a class="is-submenu" href="_components-layout-navbar-fade-dark.html">Fade dark</a></li>
                        <li><a class="is-submenu" href="_components-layout-navbar-static-light.html">Static
                                light</a></li>
                        <li><a class="is-submenu" href="_components-layout-navbar-static-dark.html">Static
                                dark</a></li>
                        <li><a class="is-submenu" href="_components-layout-navbar-static-solid.html">Static
                                solid</a></li>
                        <li><a class="is-submenu" href="_components-layout-navbar-double-dark.html">Double
                                dark</a></li>
                        <li><a class="is-submenu" href="_components-layout-navbar-double-light.html">Double
                                light</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link has-new" href="#"><span class="material-icons">view_stream</span>Sections</a>
                    <ul>
                        <li><a class="is-submenu" href="_components-sections-features.html">Features</a></li>
                        <li><a class="is-submenu" href="_components-sections-pricing.html">Pricing</a></li>
                        <li><a class="is-submenu" href="_components-sections-team.html">Team</a></li>
                        <li><a class="is-submenu" href="_components-sections-testimonials.html">Testimonials</a></li>
                        <li><a class="is-submenu" href="_components-sections-clients.html">Clients</a></li>
                        <li><a class="is-submenu" href="_components-sections-faq.html">FAQ</a></li>
                        <li><a class="is-submenu" href="_components-sections-counters.html">Counters</a></li>
                        <li><a class="is-submenu" href="_components-sections-carousel.html">Carousel</a></li>
                        <li><a class="is-submenu" href="_components-sections-cta.html">Call To Action</a></li>
                        <li><a class="is-submenu" href="_components-sections-posts.html">Posts</a></li>
                        <li><a class="is-submenu" href="_components-sections-video.html">Video</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">playlist_add_check</span>Basic
                        UI</a>
                    <ul>
                        <li><a class="is-submenu" href="_components-basicui-cards.html">Cards</a></li>
                        <li><a class="is-submenu" href="_components-basicui-buttons.html">Buttons</a></li>
                        <li><a class="is-submenu" href="_components-basicui-dropdowns.html">Dropdowns</a></li>
                        <li><a class="is-submenu" href="_components-basicui-lists.html">Lists</a></li>
                        <li><a class="is-submenu" href="_components-basicui-modals.html">Modals</a></li>
                        <li><a class="is-submenu" href="_components-basicui-tabs.html">Tabs & pills</a></li>
                        <li><a class="is-submenu" href="_components-basicui-accordion.html">Accordions</a></li>
                        <li><a class="is-submenu" href="_components-basicui-badges.html">Badges & labels</a></li>
                        <li><a class="is-submenu" href="_components-basicui-popups.html">Popups</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">playlist_add</span>Advanced
                        UI</a>
                    <ul>
                        <li><a class="is-submenu" href="_components-advancedui-tables.html">Tables</a></li>
                        <li><a class="is-submenu" href="_components-advancedui-timeline.html">Timeline</a></li>
                        <li><a class="is-submenu" href="_components-advancedui-boxes.html">Boxes</a></li>
                        <li><a class="is-submenu" href="_components-advancedui-messages.html">Messages</a></li>
                        <li><a class="is-submenu" href="_components-advancedui-megamenu.html">Megamenu</a></li>
                        <li><a class="is-submenu" href="_components-advancedui-calendar.html">Calendar</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link has-new" href="#"><span class="material-icons">check_box</span>Forms</a>
                    <ul>
                        <li><a class="is-submenu" href="_components-forms-inputs.html">Inputs</a></li>
                        <li><a class="is-submenu" href="_components-forms-controls.html">Controls</a></li>
                        <li><a class="is-submenu" href="_components-forms-layouts.html">Form layouts</a></li>
                        <li><a class="is-submenu" href="_components-forms-steps.html">Step forms</a></li>
                        <li><a class="is-submenu" href="_components-forms-uploader.html">Uploader</a></li>
                    </ul>
                </li>
                <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">adjust</span>Icons</a>
                    <ul>
                        <li><a class="is-submenu" href="_components-icons-im.html">Icons Mind</a></li>
                        <li><a class="is-submenu" href="_components-icons-sl.html">Simple Line Icons</a></li>
                        <li><a class="is-submenu" href="_components-icons-fa.html">Font Awesome</a></li>
                        <li><a class="is-submenu" href="https://material.io/icons/" target="_blank">Material Icons</a></li>
                        <li><a class="is-submenu" href="_components-extensions-iconpicker.html">Icon Picker</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /Navigation menu -->
    </div>
    <!-- /Side navigation -->
    <!-- Back To Top Button -->
    <div id="backtotop"><a href="#"></a></div>
    <div id="style-switcher" class="style-switcher visible">
        <div class="switcher-close">
            <i class="material-icons">close</i>
        </div>
        <!--Main Theme-->
        <div class="style-dot">
            <input type="radio" id="core" name="theme_selector" checked>
            <div class="style-dot-inner"></div>
        </div>
        <!--Main Theme-->
        <div class="style-dot">
            <input type="radio" id="purple" name="theme_selector">
            <div class="style-dot-inner is-purple"></div>
        </div>
        <!--Main Theme-->
        <div class="style-dot">
            <input type="radio" id="teal" name="theme_selector">
            <div class="style-dot-inner is-teal"></div>
        </div>
        <!--Main Theme-->
        <div class="style-dot">
            <input type="radio" id="green" name="theme_selector">
            <div class="style-dot-inner is-green"></div>
        </div>

        <!--Main Theme-->
        <div class="style-dot">
            <input type="radio" id="azur" name="theme_selector">
            <div class="style-dot-inner is-azur"></div>
        </div>
        <!--Main Theme-->
        <div class="style-dot">
            <input type="radio" id="blue" name="theme_selector">
            <div class="style-dot-inner is-blue"></div>
        </div>
        <!--Main Theme-->
        <div class="style-dot">
            <input type="radio" id="night" name="theme_selector">
            <div class="style-dot-inner is-night"></div>
        </div>
        <!--Main Theme-->
        <div class="style-dot">
            <input type="radio" id="yellow" name="theme_selector">
            <div class="style-dot-inner is-yellow"></div>
        </div>
        <!--Main Theme-->
        <div class="style-dot">
            <input type="radio" id="orange" name="theme_selector">
            <div class="style-dot-inner is-orange"></div>
        </div>
        <!--Main Theme-->
        <div class="style-dot">
            <input type="radio" id="red" name="theme_selector">
            <div class="style-dot-inner is-red"></div>
        </div>
    </div> <!-- Bulchat Button -->
    <div id="bulchat" class="open">
        <div class="chat-button open g-item"></div>
    </div> <!-- Chat widget -->
    <div id="chat-widget">
        <div class="chat-widget-body is-closed">
            <div class="chat-header">
                <div class="close-chat is-hidden-desktop is-hidden-tablet"><img src="assets/img/graphics/legacy/close-small.svg" alt=""></div>
                <div class="chat-team">
                    <div class="team-member has-text-centered">
                        <img src="https://via.placeholder.com/250x250" alt="" data-demo-src="assets/img/avatars/alan.jpg">
                        <div class="is-handwritten">Alan maynard</div>
                    </div>
                </div>
                <div class="response-delay has-text-centered">
                    Answers in less than 18 hours
                </div>
            </div>
            <div class="message-container">
                <div class="divider">
                    <span class="before-divider"></span>
                    <div class="children">Today</div>
                    <span class="after-divider"></span>
                </div>
                <div class="chat-message from">
                    <img src="https://via.placeholder.com/250x250" alt="" data-demo-src="assets/img/avatars/alan.jpg">
                    <div class="bubble-wrapper">
                        <div class="timestamp">02:49 pm</div>
                        <div class="chat-bubble">
                            Hey iam Alan ! Iam here to help. What can i do for you ?
                        </div>
                    </div>
                </div>
                <div class="chat-message to">
                    <div class="bubble-wrapper">
                        <div class="timestamp">02:48 pm</div>
                        <div class="chat-bubble">
                            Hello, someone out there ? I could use some help
                        </div>
                    </div>
                    <img src="https://via.placeholder.com/250x250" alt="" data-demo-src="assets/img/avatars/helen.jpg">
                </div>
            </div>
            <div class="message-input">
                <textarea class="" rows="1" placeholder="Send a message ..."></textarea>
                <div class="message-options">
                    <div class="emoji-button"></div>
                    <div class="attach-button"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Chat widget -->
    <script src="{{asset('main/mapbox-gl-js/v1.12.0/mapbox-gl.js')}}"></script>
    <script src="{{asset('main/assets/js/app.js')}}"></script>
    <script src="{{asset('main/assets/js/core.js')}}"></script>
</body>

</html>
