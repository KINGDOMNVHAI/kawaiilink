<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'make_profile_and_list_url_just_got_simple' => 'Tạo CV cá nhân và danh sách URL thật đơn giản',
    'learn_to_express_yourself_better_and_faster' => 'Học cách thể hiện bản thân tốt hơn và nhanh hơn',

    'made_by_business_people_for_business_people' => 'Do doanh nhân vì doanh nhân',
    'free_design_for_free_idea' => 'Thiết kế miễn phí cho ý tưởng tự do',
    'free_design_for_free_idea_present' => 'Bạn là một YouTuber? Hay bạn là một ứng viên cần tìm việc làm? Bạn muốn có một profile, portfolio gây ấn tượng cho các fan hâm mộ hay nhà tuyển dụng? KawaiiLink sẽ giúp bạn tạo một hồ sơ đầy đủ và miễn phí.',

    'grow_your_personal_branding' => 'Phát triển thương hiệu cá nhân của bạn',
    'and_start_making_money' => 'Và bắt đầu kiếm tiền',

    'username' => 'Tên đăng nhập',
    'password' => 'Mật khẩu',
    'profile' => 'Profile',
    'free' => 'Miễn phí',
    'simple' => 'Đơn giản',
    'cloud_sync' => 'Lưu trữ đám mây',
    'sign_up' => 'Sign Up',
    'sign_in' => 'Sign In',
    'visit_demo' => 'Xem Mẫu',
    'visit_demo_no_register' => 'Xem Mẫu (Không Cần Đăng Ký)',

    'take_control_over_your_profile_and_portfolio' => 'Kiểm soát hồ sơ và các dự án của bạn bằng cách triển khai một phiên bản cá nhân với đầy đủ tính năng.',

];
