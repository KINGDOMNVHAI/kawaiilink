<?php
use Illuminate\Support\Facades\Route;

Route::get('/change-locale/{locale}', 'main\MainController@locale')->name('change-locale');

Route::get('/', 'main\MainController@index')->name('main-home');

Route::get('/register', 'multistep\MultiStepController@index')->name('main-register');

Route::post('/register-insert', 'multistep\MultiStepController@register')->name('register-insert');



Route::get('/demo-portfolio', 'main\MainController@demo')->name('demo-portfolio-get');


// Route::get('/portfolio', 'portfolio\PortfolioAboutMeController@index')->name('main-portfolio-about-me');
