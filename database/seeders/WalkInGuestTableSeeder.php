<?php
namespace Database\Seeders;

use App\Models\WalkInGuest;
use Illuminate\Database\Seeder;

class WalkInGuestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WalkInGuest::create([
            'name_wig'      => 'walk in guest 1',
            'country_wig'   => '日本',
            'email_wig'     => 'kawaiilink@gmail.com',
            'phone_wig'     => '123456789',
        ]);
    }
}
