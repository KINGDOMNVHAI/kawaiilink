<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lastname');
            $table->string('firstname');
            $table->string('username')->unique();
            $table->string('password');
            $table->integer('gender');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->boolean('enable_user')->default(ENABLE);

            $table->string('country')->nullable();
            $table->string('address')->nullable();
            $table->string('description')->nullable();
            $table->string('signature')->nullable();
            $table->string('avatar')->nullable();
            $table->string('banner')->nullable();
            $table->rememberToken();

            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('youtube')->nullable();
            $table->string('onlyfans')->nullable();

            // $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            // $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
