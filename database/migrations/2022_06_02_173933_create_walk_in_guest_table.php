<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWalkInGuestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::create('walk_in_guest', function (Blueprint $table) {
            $table->increments('id_wig');
            $table->string('name_wig')->nullable();
            $table->string('country_wig')->nullable();
            $table->string('email_wig')->nullable();
            $table->string('phone_wig')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('walk_in_guest');
    }
}
